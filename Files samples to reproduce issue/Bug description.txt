imGetTimeInfo8 selector (SDKGetTimeInfo8 function) should be called and set 01:02:03:04 timecode or 05:06:07:08

That only happens when File structure is not well formed
Otherwise Premiere Pro decodes the timecode it finds (=> it won't have 01:02:03:04 or 05:06:07:08 timecode set by SDKGetTimeInfo8 because imGetTimeInfo8 will never get called)

Example with .MOV but it could work with other files formats (.AVI on Windows for example)

I'm not sure I am able to not give back a file handler (SDKOpenFile8 
    imFileRef       *SDKfileRef) to Premiere Pro because then it will consider the file is not opened and the importer won't work anymore
	
	
during the file import, a method ImporterHost`ML::FileImporter::ReadTimeInfo() is called
which is implemented (approximately) as follows:
if (!ML::FileImporter::ReadXMPTimeInfo(...)) {
  ML::FileImporter::ReadLegacyTimeInfo(...);
}
imGetTimeInfo8 is only called as a part of ReadLegacyTimeInfo().
If there is a XMP time info, then ReadLegacyTimeInfo is not called.
Now, the files themselves do not contain any XMP
But, according to XMP specification itself, there are some synthesized XMP tags based on native MP4 headers.


The XMP spec specifically says about timescale and duration - the native MP4 headers are mapped to <xmpDM:duration> XML property
But it says nothing about the timecode, though - so the spec doesn't fully explain this behavior

the XMP spec relationship - is currently not confirmed - the timecode itself is not even a standard MP4 header...


https://wwwimages2.adobe.com/content/dam/acom/en/devnet/xmp/pdfs/XMP%20SDK%20Release%20cc-2016-08/XMPSpecificationPart3.pdf
Reconciling native metadata with XMP
"The XMP is reconciled with native metadata from the moov-level mvhd and udta/cprt boxes. The native values are always imported, taking precedence over the XMP. Individual native items are either always updated when the XMP is updated, or are never updated from the XMP, as noted below in each case."1


"The mvhd timescale and duration are always used to compute a value for xmpDM:duration, with enough fractional digits to cover the time scale; for example, a timescale of 60 (1/60th of a second) corresponds to two fractional digits (1/100th of a second) in the XMP. The mvhd timescale and duration are never updated from the XMP."